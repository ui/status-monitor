#!/usr/bin/env python
from setuptools import setup

requirements = ["zocalo", "ispyb", "setuptools", "pyyaml", "marshmallow", "junit_xml"]
setup_requirements = []
test_requirements = ["pytest", "pytest-cov"]

setup(
    author="ESRF BCU",
    author_email="bcu@esrf.fr",
    classifiers=[
        "Development Status :: 3 - Alpha",
        "Intended Audience :: Developers",
        "License :: OSI Approved :: BSD License",
        "Natural Language :: English",
        "Programming Language :: Python :: 3",
        "Programming Language :: Python :: 3.7",
        "Programming Language :: Python :: 3.8",
        "Programming Language :: Python :: 3.9",
    ],
    description="Infrastructure status monitor",
    entry_points={"console_scripts": ["status_monitor = status_monitor.run:run"]},
    install_requires=requirements,
    license="BSD license",
    include_package_data=True,
    keywords="infrastructure tests",
    name="status-monitor",
    packages=["status_monitor"],
    package_dir={"": "src"},
    python_requires=">=3.7",
    setup_requires=setup_requirements,
    test_suite="tests",
    tests_require=test_requirements,
    url="https://gitlab.esrf.fr/ui/status-monitor",
    zip_safe=False,
)
