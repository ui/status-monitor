import os
import importlib
import time
import json
from datetime import datetime
from dataclasses import asdict

import yaml
import junit_xml

from status_monitor.tests.test import TestResult, TestResultSchema, TestStatus


def load_config():
    config_yml = os.environ.get("STATUS_MONITOR_CONFIG", "examples/tests.yml")
    if not os.path.exists(config_yml):
        raise AttributeError(f"Cannot find config file: {config_yml}")

    config = {}
    with open(config_yml, "r") as stream:
        config = yaml.safe_load(stream)

    return config


def run():
    config = load_config()

    collected_tests = {}
    collected = 0
    for name, tests in config.items():
        for config in tests:
            try:
                mod = importlib.import_module(f"status_monitor.tests.{config['type']}")
            except ModuleNotFoundError:
                print(f"Couldnt load module {config['type']}")
            else:
                if name not in collected_tests:
                    collected_tests[name] = []

                cls = getattr(mod, config["type"].title())
                inst = cls(config)
                collected_tests[name].append(inst)
                collected += 1

    print(f"Collected {collected} tests")

    results = []
    failed = 0
    allow_failed = 0
    passed = 0
    for group, test_instances in collected_tests.items():
        for test in test_instances:
            start = time.time()
            status_code, response = test.run()
            if status_code == 0:
                passed += 1
            else:
                if test.allow_fail:
                    allow_failed += 1

                allowed = "(allowed)" if test.allow_fail else ""
                print(f"- Test failed {allowed} for: '{test.name}' in '{group}'")
                print(f"  {response}")

                failed += 1

            results.append(
                TestResult(
                    success=TestStatus.SUCCESS if status_code == 0 else TestStatus.FAIL,
                    allow_fail=test.allow_fail,
                    name=test.name,
                    group=group,
                    type=test.__class__.__name__.lower(),
                    time=datetime.now(),
                    duration=time.time() - start,
                    response=response,
                )
            )

    output = {}
    sch = TestResultSchema()
    for result in results:
        if result.group not in output:
            output[result.group] = []

        marshalled_output = sch.dump(asdict(result))
        output[result.group].append(marshalled_output)

    with open("output.json", "w") as out:
        out.write(json.dumps(output, indent=2))

    with open("output.xml", "w") as f:
        junit_xml.to_xml_report_file(
            f,
            [
                junit_xml.TestSuite(
                    "status_monitor", [result.as_junit() for result in results]
                )
            ],
            prettyprint=True,
        )

    print(f"{passed} passed, {failed} failed ({allow_failed} allowed to fail)")
    if failed > allow_failed:
        exit(1)
