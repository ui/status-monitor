import requests

from marshmallow import fields

from status_monitor.tests.test import Test, TestSchema


class RequestSchema(TestSchema):
    url = fields.Url(required=True)


class Request(Test):
    schema = RequestSchema

    def run(self):
        try:
            resp = requests.get(
                self._config["url"], verify=self._config.get("verify_ssl", True)
            )
            if resp.status_code == 200:
                return 0, ""
            else:
                return 1, resp.status_code

        except Exception as e:
            return 1, str(e)
