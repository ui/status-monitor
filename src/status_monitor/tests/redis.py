from marshmallow import fields
import redis

from status_monitor.tests.test import Test, TestSchema


class RedisSchema(TestSchema):
    url = fields.Str(required=True)
    password = fields.Str()


class Redis(Test):
    schema = RedisSchema

    def run(self):
        try:
            host, port = self._config["url"].split(":")
            conn = redis.Redis(host, port, password=self._config.get("password"))
            conn.ping()

            return 0, ""

        except Exception as e:
            return 1, str(e)
