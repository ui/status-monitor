import subprocess
import shlex

from marshmallow import fields

from status_monitor.tests.test import Test, TestSchema


class PingSchema(TestSchema):
    host = fields.Str(required=True)


class Ping(Test):
    schema = PingSchema

    def run(self):
        ping = subprocess.Popen(
            shlex.split(f"ping -c 1 {self._config['host']}"),
            stderr=subprocess.PIPE,
            stdout=subprocess.PIPE,
        )
        out, err = ping.communicate()
        if ping.returncode == 0:
            return 0, ""
        else:
            return 1, err
