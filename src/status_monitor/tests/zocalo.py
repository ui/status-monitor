import sys
import time
import threading

from marshmallow import fields
import workflows
import zocalo.configuration

from status_monitor.tests.test import Test, TestSchema


class ZocaloSchema(TestSchema):
    host = fields.Str(required=True)
    services = fields.List(fields.Str(), required=True)
    timeout = fields.Int()


class Zocalo(Test):
    schema = ZocaloSchema

    def run(self):
        try:
            self._lock = threading.RLock()
            self._services = {}

            zc = zocalo.configuration.from_file()
            zc.activate()
            transport = workflows.transport.lookup("stomp")()
            transport.connect()

            transport.subscribe_broadcast(
                "transient.status", self.update_status, retroactive=True
            )

            timeout = self._config.get("timeout", 10)
            time.sleep(timeout)

            services = sorted(self._services.keys())
            searched = sorted(self._config["services"])

            missing = list(set(searched) - set(services))
            if not missing:
                return 0, ""
            else:
                extra = list(set(services) - set(searched))
                return (
                    1,
                    f"Some services were not found wihtin timeout {timeout}: {missing}, extras found: {extra}",
                )

        except Exception as e:
            return 1, str(e)

    def update_status(self, header, message):
        with self._lock:
            if not message.get("zocalo"):
                return

            self._services[message["serviceclass"]] = {
                "workflows": message["workflows"],
                "zocalo": message["zocalo"],
                "statustext": message["statustext"],
            }
