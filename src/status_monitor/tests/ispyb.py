from marshmallow import fields
import ispyb

from status_monitor.tests.test import Test, TestSchema


class ISPyBSchema(TestSchema):
    config = fields.Str(required=True)
    proposal_code = fields.Str(required=True)
    proposal_number = fields.Str(required=True)


class Ispyb(Test):
    schema = ISPyBSchema

    def run(self):
        try:
            ispyb_config = self._config["config"]
            i = ispyb.open(ispyb_config)
            result = i.core.retrieve_proposal_title(
                self._config["proposal_code"], self._config["proposal_number"]
            )

            if len(result):
                return 0, str(result[0]["title"])
            else:
                return 1, "Proposal not found"

        except Exception as e:
            return 1, str(e)
