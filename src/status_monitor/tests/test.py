from enum import IntEnum
from dataclasses import dataclass
from datetime import datetime
from abc import abstractmethod, ABC

from marshmallow import Schema, ValidationError, INCLUDE, fields
from junit_xml import TestCase


class TestSchema(Schema):
    pass


class Test(ABC):
    schema = None

    def __init__(self, config):
        if self.schema:
            try:
                sch_inst = self.schema()
                sch_inst.load(config, unknown=INCLUDE)
            except ValidationError:
                print(f"Error loading test configuration: {config}")
                raise
        else:
            pass

        self._config = config

    @property
    def name(self):
        return self._config["name"]

    @property
    def allow_fail(self):
        return self._config.get("allow_fail", False)

    @abstractmethod
    def run(self):
        pass


class TestStatus(IntEnum):
    SUCCESS = 1
    FAIL = 0


@dataclass(frozen=True)
class TestResult:
    success: TestStatus
    allow_fail: bool
    type: str
    name: str
    group: str
    response: str
    time: datetime
    duration: int

    def as_junit(self):
        jxml = TestCase(self.name, self.group, self.duration)
        if self.success == TestStatus.FAIL:
            if self.allow_fail:
                jxml.add_skipped_info(message=self.response)
            else:
                jxml.add_failure_info(message=self.response)
        return jxml


class TestResultSchema(Schema):
    success = fields.Int()
    type = fields.Str()
    name = fields.Str()
    response = fields.Str()
    time = fields.DateTime()
    duration = fields.Float()

    class Meta:
        datetimeformat = "%d-%m-%Y %H:%M:%S"
