from marshmallow import fields
from celery import Celery as CeleryApp

from status_monitor.tests.test import Test, TestSchema


class CelerySchema(TestSchema):
    broker_url = fields.Str(required=True)
    backend_url = fields.Str(required=True)


class Celery(Test):
    schema = CelerySchema

    def run(self):
        try:
            app = CeleryApp(
                "tasks",
                broker=self._config["broker_url"],
                backend=self._config["backend_url"],
            )

            inspect = app.control.inspect()
            workers = inspect.stats()

            if not workers:
                raise RuntimeError("No workers running")

            return 0, ""

        except Exception as e:
            return 1, str(e)
