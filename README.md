# Infrastructure Monitoring

Monitor infrastructure using simple tests

* Ping a host
* Make a http request
* Check a database is available
* Check zocalo services are available

Test results are outputted in json format to be ingested in a basic web status page

## Installation

```bash
pip install -e .
cp examples/tests.yml tests.yml
```

## Configuration

Define some tests:

Ping a host:
```yaml
mygroup:
  - type: ping
    host: localhost
    name: Ping localhost
```

Make a request:
```yaml
mygroup:
  - type: request
    url: https://localhost
    name: Test localhost server
    # optionally allow this test to fail
    allow_fail: true
```
