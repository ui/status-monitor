import os

from status_monitor.run import run


def test_simple():
    os.environ[
        "STATUS_MONITOR_CONFIG"
    ] = f"{os.path.dirname(__file__)}/config/tests.yml"
    run()
